(ns challenge-10
  (:require
   [util :refer [get-input ->int]]
   [clojure.string :as str]))

(def dirs
  {:north [0 -1]
   :east  [1  0]
   :south [0  1]
   :west  [-1 0]})

(def ->dirs
  (zipmap (vals dirs) (keys dirs)))

(def pipes
  {\| {:north :north
       :south :south}
   \- {:east  :east
       :west  :west}
   \L {:south :east
       :west  :north}
   \J {:south :west
       :east  :north}
   \F {:west  :south
       :north :east}
   \7 {:east  :south
       :north :west}})

(defn parse-input [i]
  (loop [acc {:grid {}
              :start nil}
         [x y] [0 0]
         i i]
    (let [c (first i)]
      (cond (nil? c)
            acc

            (= \newline c)
            (recur (assoc-in acc [:grid x y] c)
                   [0 (inc y)]
                   (rest i))

            (= \S c)
            (recur (-> (assoc acc :start [x y])
                       (assoc-in [:grid x y] c))
                   [(inc x) y]
                   (rest i))

            :else
            (recur (assoc-in acc [:grid x y] c)
                   [(inc x) y]
                   (rest i))))))

(defn neighbor-coords [[x y]]
  [[(dec x) y] [(inc x) y]
   [x (dec y)] [x (inc y)]])

(defn neighbors [s g]
  (for [c (neighbor-coords s)
        :let [dir (get ->dirs (mapv - c s))
              p (get-in g c)]
        :when p]
    {:coord c
     :dir dir
     :pipe p}))

(defn follow-pipe [loc g {:keys [coord
                                 dir
                                 pipe]}]
  (loop [path [loc coord]]
    (let [[z y] (take 2 (reverse path))
          dir (get ->dirs (mapv - z y))
          p (get-in g z)
          ndir (get (get pipes p) dir)
          ncoord (mapv + z (get dirs ndir))]
      (cond (= \S
               (get-in g ncoord))
            path
            :else (recur (conj path ncoord))))))

(defn part-a []
  (let [{s :start
         g :grid} (-> (get-input "10-a")
                      parse-input)
        n (->> (neighbors s g)
               (remove (fn [{p :pipe
                             d :dir}] (nil? (get (get pipes p) d)))))]

    (->> n
         (map #(follow-pipe s g %))
         (map #(zipmap % (range)))
         (apply (partial merge-with min))
         vals
         (apply max))))

(comment (part-a))
;; => 6823
;; =>
;; => QUIT
;; => [[1 1] ([[2 1] :east \-] [[1 2] :south \|])]
;; => ([[0 1] \.] [[2 1] \-] [[1 0] \.] [[1 2] \|])

(defn part-b []
  (let [{s :start
         g :grid} (-> (get-input "10-example-3")
                      parse-input)
        n (->> (neighbors s g)
               (remove (fn [{p :pipe
                             d :dir}] (nil? (get (get pipes p) d)))))]
    (map #(get-in g %) (follow-pipe s g (first n)))))

(part-b)
;; => [[1 1] [2 1] [3 1] [4 1] [5 1] [6 1] [7 1] [8 1] [9 1] [9 2] [9 3] [9 4] [9 5] [9 6] [9 7] [8 7] [7 7] [6 7] [6 6] [6 5] [7 5] [8 5] [8 4] [8 3] [8 2] [7 2] [6 2] [5 2] [4 2] [3 2] [2 2] [2 3] [2 4] [2 5] [3 5] [4 5] [4 6] [4 7] [3 7] [2 7] [1 7] [1 6] [1 5] [1 4] [1 3] [1 2]]
;; => [[1 1] [2 1] [3 1] [4 1] [5 1] [6 1] [7 1] [8 1] [9 1] [9 2] [9 3] [9 4] [9 5] [9 6] [9 7] [8 7] [7 7] [6 7] [6 6] [6 5] [7 5] [8 5] [8 4] [8 3] [8 2] [7 2] [6 2] [5 2] [4 2] [3 2] [2 2] [2 3] [2 4] [2 5] [3 5] [4 5] [4 6] [4 7] [3 7] [2 7] [1 7] [1 6] [1 5] [1 4] [1 3] [1 2]]
