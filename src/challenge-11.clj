(ns challenge-11
  (:require
   [util :refer [get-input print-grid ->grid manhattan-distance]]
   [clojure.math.combinatorics :refer [combinations]]
   [clojure.string :as str]
   [clojure.set :as set]))

(defn transpose [vs]
  (apply mapv vector vs))

(defn find-in-grid [p g]
  (for [x (-> g first count range)
        y (-> g count range)
        :let [el (-> (get g y) (get x))]
        :when (= p el)]
    [x y]))

(defn expand-rows [m]
  (reduce
   (fn [acc row]
     (if (every? #(= \. %) row)
       (concat acc [row row])
       (concat acc [row])))
   []
   m))

(defn expand-galaxy [g]
  (->> g
       expand-rows
       transpose
       expand-rows
       transpose))

(defn part-a []
  (let [g (-> (get-input "11-example") ->grid expand-galaxy)
        coords (find-in-grid \# g)]
    (->> (combinations coords 2)
         (map (fn [[a b]] [[a b] (manhattan-distance a b)]))
         (map second)
         (reduce +))))

(part-a)
;; => 374

(defn empty-sectors [locs]
  (let [xs (into #{} (map first locs))
        ys (into #{} (map second locs))]
    [(set/difference (into #{} (range (apply min xs) (apply max xs))) xs)
     (set/difference (into #{} (range (apply min ys) (apply max ys))) ys)]))

(defn voids-crossed [[a b] [c d] [xs ys]]
  [(->> xs
        (filter #(< (min a c) % (max a c)))
        count)
   (->> ys
        (filter #(< (min b d) % (max b d)))
        count)])

(defn part-b []
  (let [g (-> (get-input "11-example") ->grid)
        coords (find-in-grid \# g)
        voids (empty-sectors coords)
        expansion-factor 2]
    (->> (combinations coords 2)
         (map (fn [[a b]] (+ (manhattan-distance a b)
                            (->> (voids-crossed a b voids)
                                 (reduce +)
                                 (* (dec expansion-factor))))))
         (reduce +))))

(part-b)
;; => 483844716556
;; => 8410
;; => 1030
;; => 374
