(ns challenge-5
  (:require
   [util :refer [get-input ->int]]
   [clojure.string :as str]))

(defn range-line->entry [[to-start from-start r]]
  [from-start to-start r])

(def label-p (re-pattern #"(?<from>\w+)-to-(?<to>\w+)"))
(defn parse-map [c]
  (let [[label-str ranges] (str/split c #":")
        [_ from to] (re-find label-p label-str)]
    {[(keyword from) (keyword to)]
     (->> ranges
          str/trim
          str/split-lines
          (map #(re-seq #"\d+" %))
          (map (fn [c] (map ->int c)))
          (map range-line->entry)
          (sort-by first)
          (into []))}))

(defn parse-seeds [c]
  (let [[_ nums] (str/split c #":" 1)]
    (->> (re-seq #"\d+" c)
         (map ->int))))

(defn parse-input [i]
  (let [[seeds & maps] (str/split i #"\n\n")]
    [(parse-seeds seeds)
     (->> (map parse-map maps)
          (apply merge))]))

(def key-seq [[:seed :soil]
              [:soil :fertilizer]
              [:fertilizer :water]
              [:water :light]
              [:light :temperature]
              [:temperature :humidity]
              [:humidity :location]])

(def find-next
  (memoize
   (fn [mapper ptr]
     (reduce (fn [ptr [idx to-idx offset]]
               (cond (< ptr idx)
                     (reduced ptr)

                     (and (>= ptr idx)
                          (<= ptr (+ idx offset)))
                     (let [dif (- ptr idx)]
                       (reduced (+ to-idx dif)))

                     :else ptr))
             ptr
             mapper))))

(defn follow-guide [{:keys [guide ptr] :as acc}
                    step]
  (let [mapper   (get guide step)
        next-ptr (find-next mapper ptr)]
    (assoc acc :ptr next-ptr)))

(defn seed->loc [guide seed]
  (let [g {:guide guide
           :ptr seed}]
    (-> (reduce follow-guide g key-seq)
        :ptr)))

(defn part-a []
  (let [[seeds guide] (->> (get-input "05-a")
                           parse-input)]
    (apply min (for [s seeds]
                 (seed->loc guide s)))))

(comment (part-a))
;; => 240320250
;; => [(79 14 55 13) (82 43 86 35)]
;; => ["seeds: 79 14 55 13" ("seed-to-soil map:\n50 98 2\n52 50 48" "soil-to-fertilizer map:\n0 15 37\n37 52 2\n39 0 15" "fertilizer-to-water map:\n49 53 8\n0 11 42\n42 0 7\n57 7 4" "water-to-light map:\n88 18 7\n18 25 70" "light-to-temperature map:\n45 77 23\n81 45 19\n68 64 13" "temperature-to-humidity map:\n0 69 1\n1 0 69" "humidity-to-location map:\n60 56 37\n56 93 4")]

(defn range->next-range
  ([r m]
   (range->next-range r m []))
  ([[a b] [s d n] acc]
   (cond (>= 0 (- b a))
         [nil acc]

         (nil? s)
         [[a b] acc]

         (< a s)
         (let [run (min (- s a) b)]
           (recur [(+ a run) b]
                  [s d n]
                  (conj acc [a run])))

         (and (>= a s)
              (< a (+ s n)))
         (let [run (min (- (+ s n) a) (- b a))
               offset (- a s)
               init (+ d offset)]
           (recur [(+ a run) b]
                  nil
                  (conj acc [init (+ init run)])))
         :else
         [[a b] acc])))

(defn range->next-ranges [[a b] mapper]
  (loop [[a b] [a b]
         m mapper
         acc []]
    (cond
      (nil? a)
      acc

      (empty? m)
      (conj acc [a b])

      :else
      (let [[[c d] segments] (range->next-range [a b] (first m))]
        (recur [c d] (rest m) (concat acc segments))))))

(defn coalesce-ranges [coll]
  (let [sorted (sort-by (juxt first second) coll)]
    (reduce (fn [acc [c d]]
              (let [[a b] (peek acc)
                    overlap? (> b c)
                    nacc (if overlap? (pop acc) acc)
                    e (if overlap? (min a c) c)
                    f (if overlap? (max b d) d)]
                (conj nacc [e f])))
            [(first sorted)]
            (rest sorted))))

(coalesce-ranges [[0 1] [2 10] [8 16]])
;; => [[0 1] [2 16]]
;; =>

(defn map-range [{:keys [guide ranges] :as acc}
                 s]
  (println "STEP: " (str s))
  (let [sorted (coalesce-ranges ranges)
        mapper (get guide s)
        nranges (mapcat #(range->next-ranges % mapper) sorted)]
    (assoc acc :ranges nranges)))

(defn map-ranges [ranges guide]
  (let [g {:guide guide
           :ranges ranges}]
    (->> (reduce map-range g key-seq)
         :ranges
         (sort-by (juxt first second)))))

(defn part-b []
  (let [[seeds guide] (->> (get-input "05-a")
                           parse-input)
        ranges (->> seeds
                    (partition 2)
                    (map (fn [[a b]] [a (+ a b)])))]
    (->> (map-ranges ranges guide)
         ffirst)))

(part-b)
;; => 28580589
;; => 46
;; => ([46 10] [56 60] [60 61] [82 85] [86 90] [94 97] [97 99])
;; => ([46 10] [56 60] [60 61] [82 85] [86 90] [94 97] [97 99])
;; =>
;; =>
