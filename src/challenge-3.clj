(ns challenge-3
  (:require
   [util :refer [get-input]]
   [clojure.string :as str]))

(def invalid-symbols #{\1 \2 \3
                       \4 \5 \6
                       \7 \8 \9
                       \0 \.})

(defn symbol-coords [i]
  (->> (str/split-lines i)
       (map-indexed (fn [idx l] [idx (map-indexed (fn [x c] [x c]) l)]))
       (map (fn [[y xs]]
              [y (remove (fn [[x c]] (invalid-symbols c)) xs)]))
       (remove (fn [[y xs]] (empty? xs)))
       (mapcat (fn [[y xs]] (map (fn [x] [(first x) y]) xs)))
       (into #{})))

(defn gear-coords [i]
  (->> (str/split-lines i)
       (map-indexed (fn [idx l] [idx (map-indexed (fn [x c] [x c]) l)]))
       (map (fn [[y xs]]
              [y (keep (fn [[x c]] (when (#{\*} c) x)) xs)]))
       (remove (fn [[y xs]] (empty? xs)))
       (mapcat (fn [[y xs]] (map (fn [x] [x y]) xs)))
       ))

(defn extract-numbers [l]
  (let [m (re-matcher #"\d+" l)]
    (loop [hit (.find m)
           nums []]
      (if hit
        (let [res (.toMatchResult m)
              start (.start res)
              end (.end res)
              num (.group res)]
          (recur (.find m) (conj nums {:start start :end end :num num})))
        nums))))

(defn boundary-points [y start-x end-x]
  (for [x1 (range (dec start-x) (inc end-x))
        y1 (range (dec y) (+ 2 y))
        :when (or (not= y1 y)
                  (and (= y1 y)
                       (or (< x1 start-x)
                           (= x1 end-x))))]
    [x1 y1]))

(defn- keep-adjacent-words [sym-coords y words]
  (for [word words
        :when (some #(contains? sym-coords %) (boundary-points y (:start word) (:end word)))]
    (:num word)))

(defn part-a []
  (let [i (get-input "03-a")
        sym-coords (symbol-coords i)]
    (->> (str/split-lines i)
         (map-indexed (fn [i l] [i (extract-numbers l)]))
         (mapcat (fn [[y words]]
                   (keep-adjacent-words sym-coords
                                        y
                                        words)))
         (map #(Integer/parseInt %))
         (reduce +))))

(comment
  (part-a))
;; => 507214 -- correct

(defn mark-gears [gears numbers]
  (reduce (fn [acc num]
            (if-let [coord (some (fn [coord]
                                   (when (contains? gears coord)
                                     coord))
                                 (boundary-points (:y num)
                                                  (:start num)
                                                  (:end num)))]
              (update acc coord conj (:num num))
              acc)) gears numbers))

(defn part-b []
  (let [i (get-input "03-a")
        gears (gear-coords i)
        gear-map (into {} (for [gear gears] [gear []]))
        numbers (->> (str/split-lines i)
                     (map-indexed (fn [i l] [i (extract-numbers l)]))
                     (mapcat (fn [[y words]] (map #(assoc % :y y) words))))]

    (->> (mark-gears gear-map numbers)
         (remove (fn [[k v]] (not= 2 (count v))))
         (map (fn [[k v]] (map #(Integer/parseInt %) v)))
         (map #(apply * %))
         (reduce +)) ))

(part-b)
;; => 72553319
