(ns challenge-6
  (:require
   [util :refer [get-input ->int]]
   [clojure.string :as str]))

(defn parse-input [i]
  (let [[times distances] (->> (str/split-lines i)
                               (map #(str/split % #":" 2))
                               (map second)
                               (map str/trim)
                               (map #(str/split % #"\s+"))
                               (map #(map ->int %)))]
    (map vector times distances)))

(defn parse-input-b [i]
  (let [[time distance] (->> (str/split-lines i)
                               (map #(str/split % #":" 2))
                               (map second)
                               (map str/trim)
                               (map #(str/replace % #"\s+" ""))
                               (map ->int))]
    [time distance]))

(defn find-first-idx
  ([[t d]]
   (find-first-idx [t d] 0))
  ([[t d] s]
   (if (> (* t s) d)
     s
     (recur [(dec t) d] (inc s)))))

(defn find-last-idx
  ([[t d]]
   (find-last-idx [0 d] t))
  ([[t d] s]
   (if (> (* t s) d)
     s
     (recur [(inc t) d] (dec s)))))

(defn record->options [[t d]]
  (inc (- (find-last-idx [t d]) (find-first-idx [t d]))))

(defn part-a []
  (->> (get-input "06-a")
       parse-input-b
       (map record->options)
       (apply *)))

(comment (part-a))
;; => 449820
;; => 288
;; => (4 8 9)

(defn part-b []
  (->> (get-input "06-a")
       parse-input-b
       record->options))

(part-b)
;; => 42250895
;; => ([7 9] [15 40] [30 200])
