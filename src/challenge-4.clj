(ns challenge-4
  (:require
   [util :refer [get-input]]
   [clojure.string :as str]
   [clojure.set :as set]
   [clojure.math :refer [pow]]))

(defn extract-groups [l]
  (let [[_ winners numbers] (re-find #"Card\s+\d+:([^\|]+)\|([^\|]+)" l)]
    [(into #{} (re-seq #"\d+" winners))
     (into #{} (re-seq #"\d+" numbers))]))

(defn part-a []
  (->> (get-input "04-a")
       str/split-lines
       (map extract-groups)
       (map #(apply set/intersection %))
       (map count)
       (remove zero?)
       (map (fn [p] (pow 2 (dec p))))
       (reduce +)))

(comment (part-a))
;; => 24160.0

(defn parse-input [l]
  (let [[_ card winners numbers] (re-find #"Card\s+(\d+):([^\|]+)\|([^\|]+)" l)]
    [(Integer/parseInt card)
     (into #{} (re-seq #"\d+" winners))
     (into #{} (re-seq #"\d+" numbers))]))

(defn track-winners [cards [num winners numbers]]
  (let [copies (get cards num)
        num-new (-> (set/intersection winners numbers) count)
        incr (partial + copies)
        new-cards (for [i (range (inc num) (+ (inc num) num-new))
                        :when (contains? cards i)]
                    i)]
    (reduce (fn [acc k]
              (update acc k incr)) cards new-cards)))

(defn part-b []
  (let [cards (->> (get-input "04-a")
                   str/split-lines
                   (map parse-input))
        copies (zipmap (map first cards)  (cycle [1]))]
    (->> (reduce track-winners copies cards)
         vals
         (reduce +))))

(part-b)
;; => 5659035
;; => 30
;; => 30
;; => {1 1, 2 2, 3 4, 4 8, 5 14, 6 1}
;; => {1 1, 2 2, 3 2, 4 2, 5 2, 6 1}
;; => 4
;; => ([1 #{"83" "17" "48" "41" "86"} #{"9" "83" "17" "48" "53" "6" "31" "86"}] [2 #{"61" "20" "13" "32" "16"} #{"82" "61" "68" "30" "19" "17" "24" "32"}] [3 #{"59" "21" "44" "53" "1"} #{"69" "14" "82" "21" "1" "63" "72" "16"}] [4 #{"69" "73" "84" "41" "92"} #{"58" "83" "51" "59" "5" "76" "84" "54"}] [5 #{"83" "87" "26" "28" "32"} #{"88" "93" "22" "82" "30" "36" "12" "70"}] [6 #{"18" "13" "56" "72" "31"} #{"67" "36" "35" "77" "74" "11" "10" "23"}])
;; => {1 1, 2 1, 3 1, 4 1, 5 1, 6 1}
