(ns challenge-16
  (:require
   [clojure.core.match :refer [match]]
   [clojure.string :as str]
   [util :refer [get-input ->int ->grid print-grid grid->size <-loc transpose manhattan-distance]]))

(def dirs
  {:u [0 -1]
   :d [0  1]
   :l [-1 0]
   :r [1  0]})

(defn path [d c]
  (match [d c]
    [_ \.]           [:no-split d]
    [(:or :l :r) \-] [:no-split d]
    [(:or :l :r) \|] [:split    [:u :d]]
    [:l          \\] [:no-split :u]
    [:r          \\] [:no-split :d]
    [:l          \/] [:no-split :d]
    [:r          \/] [:no-split :u]
    [(:or :u :d) \-] [:split    [:l :r]]
    [(:or :u :d) \|] [:no-split d]
    [:u          \\] [:no-split :l]
    [:d          \\] [:no-split :r]
    [:u          \/] [:no-split :r]
    [:d          \/] [:no-split :l]))

(defn encode-row [r]
  (->> (map-indexed (fn [i e] (when-not (#{\.} e) [i e])) r)
       (remove #(nil? %))))

(defn steps-to-edge [d [x y] g]
  (let [[max-x max-y] (grid->size g)]
    (case d
      :u y
      :d (- max-y y)
      :l x
      :r (- max-x x))))

(defn step [d pos g seen]
  (when-not (contains? seen [d pos])
    (when-let [c (<-loc g pos)]
      (let [[s? n] (path d c)]
        (if (= s? :no-split)
          (lazy-seq (concat [pos]
                            (step n (mapv + pos (dirs n)) g (conj seen [n pos]))))
          (lazy-cat [pos]
                    (step (first n) (mapv + pos (dirs (first n))) g (conj seen [(first n) pos]))
                    (step (second n) (mapv + pos (dirs (second n))) g (conj seen [(second n) pos]))))))))

(defn next-pos [d [x y] [xs ys]]
  (let [s (case d
            :u (drop-while #(> (first %) y) (reverse (nth ys y)))
            :d (drop-while #(< (first %) y) (nth ys y))
            :l (drop-while #(> (first %) x) (reverse (nth xs x)))
            :r (drop-while #(< (first %) x) (nth xs x)))
        [n c] (first s)]
    (when n
      (case d
        (:u :d) [[x n] c]
        (:l :r) [[n y] c]))))

(defn shoot [d pos {:keys [g enc seen] :as state}]
  (when (and (not (contains? seen [d pos]))
             (<-loc g pos))
    (if-let [[n-pos c] (next-pos d pos enc)]
      (let [[s? n] (path d c)]
        (if (= s? :no-split)
          (lazy-seq (concat [(manhattan-distance pos n-pos)]
                            (shoot n
                                   (mapv + n-pos (dirs n))
                                   (assoc state
                                          :seen (conj seen d pos)))))
          (lazy-cat [(manhattan-distance pos n-pos)]
                    (shoot (first n)
                           (mapv + n-pos (dirs (first n)))
                           (assoc state
                                  :seen (conj seen [(first n)
                                                    n-pos])))
                    (shoot (second n)
                           (mapv + n-pos (dirs (second n)))
                           (assoc state
                                  :seen (conj seen [(second n)
                                                    n-pos]))))))
      ;; need function for calculate steps to edge
      [(steps-to-edge d pos g)])))

(defn part-a []
  (let [g (->> (get-input "16-example")
               (->grid)
               (print-grid "example"))
        xs (mapv encode-row g)
        ys (mapv encode-row (transpose g))]
    (->> (shoot :r [0 0] {:g g
                          :enc [xs ys]
                          :seen #{}})
         (reduce +))))

(part-a)
;; => 1
;; => (([1 \|]) ([0 \|] [7 \-] [8 \|]) ([1 \-] [9 \/]) ([7 \-] [9 \/]) ([1 \\] [6 \/] [7 \/]) ([0 \\] [2 \|] [9 \|]) ([2 \-] [6 \\] [8 \-]) ([6 \\] [7 \|] [8 \|]) ([3 \|]) ([5 \\] [8 \\]))
;; => (([1 \|] [5 \\]) ([0 \|] [2 \-] [4 \\]) ([5 \|] [6 \-]) ([8 \|]) () ([9 \\]) ([4 \/] [6 \\] [7 \\]) ([1 \-] [3 \-] [4 \/] [7 \|]) ([1 \|] [6 \-] [7 \|] [9 \\]) ([2 \/] [3 \/] [5 \|]))
;; => [\. \| \. \. \. \\ \. \. \. \.]
;; => 46
