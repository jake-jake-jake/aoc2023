(ns challenge-1
  (:require
   [util :refer [get-input]]
   [clojure.string :as str]))

(defn- is-digit? [c]
  (#{\0 \1 \2 \3 \4 \5 \6 \7 \8 \9} c))

(defn word-digit [s]
  (re-seq #"(?<=(one|two|three|four|five|six|seven|eight|nine|[1-9]))" s))

(defn part-a []
  (->> (get-input "01-a")
       str/split-lines
       (map (fn [l] (keep is-digit? l)))
       (map (fn [l]
              [(first l) (last l)]))
       (map #(apply str %))
       (map #(Integer/parseInt %))
       (reduce +)))

(part-a)
;; => 55816

(defn line-reduce [l]
  (let [ms (->> l
                word-digit
                (map second)
                (remove nil?)
                )]
    [(first ms) (last ms)]))

(defn to-digs [s]
  ({"one" "1"
    "two" "2"
    "three" "3"
    "four" "4"
    "five" "5"
    "six" "6"
    "seven" "7"
    "eight" "8"
    "nine" "9"} s s))

(defn part-b []
  (->> (get-input "01-a")
       str/split-lines
       (map line-reduce)
       (map #(mapv to-digs %))
       (map #(apply str %))
       (map #(Integer/parseInt %))
       (reduce +)))

(part-b)
;; => 281
;; => 55816
