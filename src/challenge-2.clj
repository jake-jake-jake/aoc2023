(ns challenge-2
  (:require
   [util :refer [get-input]]
   [clojure.string :as str]))

(def example (get-input "02-a"))

(def game-p (re-pattern #"(?i)Game\s+(\d+)"))
(def cube-p (re-pattern #"(?i)(\d+)\s+([^,]+)"))

(defn- parse-cubes [cubes]
  (let [rounds nil]
    (->> (re-seq cube-p cubes)
         (map (fn [[_ num color]]
                [color
                 (Integer/parseInt num)]))

         (into {}))))

(defn- parse-line [l]
  (let [[game rounds] (str/split l #":")
        cubes (str/split rounds #";")]
    [(-> (re-find game-p game)
         second)
     (->> (map parse-cubes cubes))]))

(defn- possible? [cubes limits]
  (every? true? (for [[k v] cubes]
                  (<= v (get limits k)))))

(defn- all-possible? [coll limits]
  (every? true? (map #(possible? % limits) coll)))

(defn part-a []
  (let [limits {"red" 12
                "green" 13
                "blue" 14}]
    (->> (get-input "02-a")
         str/split-lines
         (map parse-line)
         (filter (fn [[g r]]
                   (all-possible? r limits)))
         (map first)
         (map #(Integer/parseInt %))
         (reduce +))))

(part-a)
;; => 1867

(defn part-b []
  (let [limits {"red" 12
                "green" 13
                "blue" 14}]
    (->> (get-input "02-a")
         str/split-lines
         (map parse-line)
         (map (fn [[g r]]
                [g
                 (apply (partial merge-with max) r)]))

         (map second)
         (map vals)
         (map #(apply * %))
         (reduce +))))

(part-b)
