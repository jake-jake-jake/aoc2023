(ns challenge-13
  (:require
   [util :refer [get-input transpose]]
   [clojure.string :as str]))

(defn find-reflection
  ([rows]
   (find-reflection rows =))
  ([rows cmp-fn]
   (loop [rem-rows (seq (rest rows))
          seen [(first rows)]]
     (when (seq rem-rows)
       (if (cmp-fn (take (count seen) rem-rows)
                   (take (count rem-rows) (reverse seen)))
         (count seen)
         (recur (rest rem-rows) (conj seen (first rem-rows))))))))

(defn parse-input [i]
  (let [chunks (->> (str/split i #"\n\n")
                    (map str/split-lines))]
    chunks))

(defn summarize [[x y]]
  (+ x (* 100 y)))

(defn smudged? [a b]
  (->> (mapcat (partial map #(if (= %1 %2) 0 1)) a b)
       (reduce +)
       (= 1)))

(defn part-a []
  (->> (get-input "13-a")
       parse-input
       (map (fn [c] [(or (find-reflection (transpose c)) 0)
                    (or (find-reflection c) 0)]))
       (reduce #(mapv + %1 %2))
       summarize))

(part-a)
;; => 26957
;; =>
;; => 16194
;; => 9030
;; => 405
;; => 405
;; => ([nil [["09a349de668e8c818657e4e298d63158"] 5]] [[["688c516c696d40cdf4f1e4da721447eb"] 4] nil])
;; => "#.##..##.\n..#.##.#.\n##......#\n##......#\n..#.##.#.\n..##..##.\n#.#.##.#.\n\n#...##..#\n#....#..#\n..##..###\n#####.##.\n#####.##.\n..##..###\n#....#..#"
;; => "#.##..##.\n..#.##.#.\n##......#\n##......#\n..#.##.#.\n..##..##.\n#.#.##.#.\n\n#...##..#\n#....#..#\n..##..###\n#####.##.\n#####.##.\n..##..###\n#....#..#"

(defn part-b []
  (->> (get-input "13-a")
       parse-input
       (map (fn [c] [(or (find-reflection (transpose c) smudged?) 0)
                    (or (find-reflection c smudged?) 0)]))
       (reduce #(mapv + %1 %2))
       summarize))
(part-b)
;; => 42695
;; => nil
;; =>
