(ns challenge-15
  (:require
   [util :refer [get-input ->int]]
   [clojure.string :as str]))

(defn hash-chr [acc c]
  (-> c
      int
      (+ acc)
      (* 17)
      (rem 256)))

(defn hash-str [s]
  (reduce hash-chr 0 s))

(defn part-a []
  (let [chunks (-> (get-input "15-a")
                   str/trim
                   (str/split #","))]
    (->> chunks
         (map (partial reduce hash-chr 0))
         (reduce +))))
;; => #'challenge-15/part-a

(part-a)
;; => 516070
;; => 516048 --whoops left newline
;; => 1320
;; => (30 253 97 47 14 180 9 197 48 214 231)

(defn parse-instruction [s]
  (let [[_ box inst lens] (re-find #"([^=-]+)(=|-)(.+)?" s)]
    {:box box
     :inst inst
     :lens lens}))

(defn add-to [v {:keys [box] :as instruction}]
  (let [index (->> (map-indexed (fn [i m] [i (= box (:box m))]) v)
                   (filter #(true? (second %)))
                   ffirst)]
    (if index
      (assoc v index instruction)
      (conj v instruction))))

(defn remove-from [v {:keys [box] :as instruction}]
  (->> v
       (filter #(not= box (:box %)))
       (into [])))

(defn do-instruction [acc {:keys [box inst] :as instruction}]
  (let [i (hash-str box)
        a (get acc i)
        n (if (= "=" inst)
            (add-to a instruction)
            (remove-from a instruction))]
    (assoc acc i n)))

(defn part-b []
  (let [instructions (-> (get-input "15-a")
                         str/trim
                         (str/split #","))
        state (->> instructions
                   (map parse-instruction)
                   (reduce do-instruction
                           (zipmap (range 256) (repeat []))))]
    (->> (for [i (range 256)
               :let [v (get state i)]
               :when (seq v)]
           (->> (map-indexed (fn [j {l :lens}]
                               (* (inc i)
                                  (inc j)
                                  (->int l))) v)
                (reduce +)))
         (reduce +))))

(part-b)
;; => 244981N
;; => 145N
;; => (5N 140N)
;; => [{:box "rn", :inst "=", :lens "1"} {:box "cm", :inst "=", :lens "2"}]
;; => [{:box "ot", :inst "=", :lens "7"} {:box "ab", :inst "=", :lens "5"} {:box "pc", :inst "=", :lens "6"}]
