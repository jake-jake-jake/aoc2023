(ns challenge-7
  (:require
   [util :refer [get-input ->int]]
   [clojure.string :as str]
   [clojure.core.match :refer [match]]))

(def card-weights
  {\A 1
   \K 2
   \Q 3
   \T 5
   \9 6
   \8 7
   \7 8
   \6 9
   \5 10
   \4 11
   \3 12
   \2 13
   \J 14})

(defn freq->weight [[f s & r]]
  (match [f s]
    [5 _] 1
    [4 _] 2
    [3 2] 3
    [3 _] 4
    [2 2] 5
    [2 _] 6
    [_ _] 7))

(defn parse-hand [l]
  (let [[hand bid] (str/split l #"\s+" 2)]
    {:hand hand
     :weight (->> (frequencies hand) vals (sort >) freq->weight)
     :bid (->int bid)}))

(defn- improve-hand [f]
  (let [[a b] (some->> (dissoc f \J) vals (sort >))]
    (if-let [jokers (get f \J)]
      [(+ (or a 0) jokers) (or b 0)]
      [a b])))

(defn parse-hand-with-jokers [l]
  (let [[hand bid] (str/split l #"\s+" 2)
        f (frequencies hand)]
    {:hand hand
     :weight (->> hand frequencies improve-hand freq->weight)
     :bid (->int bid)}))

(defn part-a []
  (->> (get-input "07-a")
       str/split-lines
       (map parse-hand)
       (sort-by (juxt :weight (fn [h] (->> h :hand (map card-weights) (into [])))))
       reverse
       (map-indexed (fn [i m] (* (inc i) (:bid m))))
       (reduce +)))

(comment part-a)
;; => 253603890
;; => ([1 "254JK"] [2 "254KA"] [3 "274Q5"] [4 "276Q5"] [5 "2786T"] [6 "27K3T"] [7 "29Q7T"] [8 "29QA6"] [9 "2T34K"] [10 "2T3J7"] [11 "2T653"] [12 "2T759"] [13 "2T895"] [14 "2Q34J"] [15 "2K94A"] [16 "2KQ69"] [17 "2A5K3"] [18 "3475K"] [19 "34K62"] [20 "352Q8"] [21 "358K7"] [22 "35J97"] [23 "35QA9"] [24 "36857"] [25 "36K97"] [26 "3TK6J"] [27 "3J247"] [28 "3J942"] [29 "42AQK"] [30 "43586"] [31 "43J86"] [32 "465AQ"] [33 "482TQ"] [34 "48JK6"] [35 "49526"] [36 "4JA57"] [37 "4QJ8T"] [38 "4QJ8A"] [39 "4A69Q"] [40 "4A76J"] [41 "4AQ9K"] [42 "523QT"] [43 "53JQA"] [44 "54K87"] [45 "54K89"] [46 "56472"] [47 "569TQ"] [48 "57T4A"] [49 "58T24"] [50 "59678"])
;; => ([1 "32T3K"] [2 "KTJJT"] [3 "KK677"] [4 "T55J5"] [5 "QQQJA"])
;; => 254462361
;; => 254462361
;; => 6440
;; => 6440
;; => ([1 "32T3K"] [2 "KTJJT"] [3 "KK677"] [4 "T55J5"] [5 "QQQJA"])
;; => ([1 "QQQJA"] [2 "T55J5"] [3 "KK677"] [4 "KTJJT"] [5 "32T3K"])
;; => ([1 "32T3K"] [2 "KK677"] [3 "KTJJT"] [4 "QQQJA"] [5 "T55J5"])
;; => ([1 "32T3K"] [2 "T55J5"] [3 "QQQJA"] [4 "KTJJT"] [5 "KK677"])
;; => ({:hand "32T3K", :frequencies [2 1 1 1], :bid "765"} {:hand "T55J5", :frequencies [3 1 1], :bid "684"} {:hand "KK677", :frequencies [2 2 1], :bid "28"} {:hand "KTJJT", :frequencies [2 2 1], :bid "220"} {:hand "QQQJA", :frequencies [3 1 1], :bid "483"})
;; => ({:hand "32T3K", :frequencies {\3 2, \2 1, \T 1, \K 1}, :bid "765"} {:hand "T55J5", :frequencies {\T 1, \5 3, \J 1}, :bid "684"} {:hand "KK677", :frequencies {\K 2, \6 1, \7 2}, :bid "28"} {:hand "KTJJT", :frequencies {\K 1, \T 2, \J 2}, :bid "220"} {:hand "QQQJA", :frequencies {\Q 3, \J 1, \A 1}, :bid "483"})
;; => ["32T3K 765" "T55J5 684" "KK677 28" "KTJJT 220" "QQQJA 483"]

(defn part-b []
  (->> (get-input "07-a")
       str/split-lines
       (map parse-hand-with-jokers)
       (sort-by (juxt :weight (fn [h] (->> h :hand (map card-weights) (into [])))))
       reverse
       (map-indexed (fn [i m] (* (inc i) (:bid m))))
       (reduce +)))

(part-b)
;; => 253630098
;; =>
;; => ([1 "32T3K"] [2 "KK677"] [3 "T55J5"] [4 "QQQJA"] [5 "KTJJT"])
;; => (765 56 2052 1932 1100)
;; => 6440
