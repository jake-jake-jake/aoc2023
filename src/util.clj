(ns util
  (:require [clojure.math.numeric-tower :refer [expt]]
            [clojure.string :as str]))

(defn print-grid
  ([g]
   (print-grid nil g))
  ([l g]
   (when l
     (println l))
   (doall
    (for [l g]
      (println (str/join l))))
   (println)
   g))

(defn ->grid [s]
  (->> s
       str/split-lines
       (mapv vec)))

(defn grid->size [g]
  [(count (first g)) (count g)])

(defn <-loc [g [x y]]
  (get-in g [y x]))

(defn manhattan-distance [& vs]
  (->> (apply map - vs)
       (map abs)
       (reduce +)))

(defn get-input [n]
  (slurp (str "./input/" n)))

(defn ->int [s]
  (-> (Long/parseLong s)
      bigint))

;; stolen from https://stackoverflow.com/questions/960980/fast-prime-number-generation-in-clojure
(defn gen-primes "Generates an infinite, lazy sequence of prime numbers"
  []
  (letfn [(reinsert [table x prime]
            (update-in table [(+ prime x)] conj prime))
          (primes-step [table d]
            (if-let [factors (get table d)]
              (recur (reduce #(reinsert %1 d %2) (dissoc table d) factors)
                     (inc d))
              (lazy-seq (cons d (primes-step (assoc table (* d d) (list d))
                                             (inc d))))))]
    (primes-step {} 2)))

(defn prime-factors [num]
  (let [primes (take-while #(<= % num) (gen-primes))]
    (loop [n num
           primes primes
           factors []]
      (let [prime (first primes)
            m (mod n prime)]
        (cond (= 1 n)
              factors
              (zero? m)
              (recur (/ n prime)
                     primes
                     (conj factors prime))
              :else
              (recur n
                     (rest primes)
                     factors))))))

(defn lcm [& nums]
  (let [factors (for [n nums]
                  (prime-factors n))
        exponents (->> factors
                       (map frequencies)
                       (apply (partial merge-with max)))]
    (->> exponents
         (map (fn [[n e]] (expt n e)))
         (reduce *))))

(defn transpose [v]
  (apply map vector v))
