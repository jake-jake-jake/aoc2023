(ns challenge-9
  (:require
   [util :refer [get-input ->int]]
   [clojure.string :as str]))

(defn parse-line [l]
  (mapv ->int (re-seq #"-?\d+" l)))

(defn next-row [r]
  (->> (partition 2 1 r)
       (map reverse)
       (mapv #(apply - %))))

(defn predict [l]
  (let [diffs (next-row l)]
    (if (every? zero? diffs)
      (last l)
      (+ (last l) (predict diffs)))))

(defn lookback [l]
  (let [diffs (next-row l)]
    (if (every? zero? diffs)
      (first l)
      (- (first l) (lookback diffs)))))

(defn make-pyramid [h]
  (loop [rows [h]]
    (let [n-row (next-row (last rows))
          n-rows (conj rows n-row)]
      (if (every? zero? n-row)
        n-rows
        (recur n-rows)))))

(defn print-pyramid [p]
  (loop [p p
         pad []]
    (let [[f r] ((juxt first rest) p)]
      (when-not (nil? f)
        (println (str (str/join pad) (str/join " " f)))
        (recur r (conj pad " "))))))
;; b - a = t
;; b = t + a
(defn extrapolate-pyramid [p]
  (let [r-pyramid (reverse p)
        plus-zero (conj (last p) 0)]
    (loop [rows (rest r-pyramid)
           extended [plus-zero]]
      (let [work-row (first rows)
            a (last work-row)
            t (last (last extended))]
        (if (nil? work-row)
          (reverse extended)
          (let [b (+ t a)]
            (recur (rest rows)
                   (conj extended (conj work-row b)))))))))

(defn part-a []
  (->> (get-input "09-a")
       str/split-lines
       (map parse-line)
       (map make-pyramid)
       (map extrapolate-pyramid)
       (map (comp last first))
       (reduce +)))

(part-a)
;; => 2038472161N
;; => 2038472161N
;; => 2038472161N
;; => 114N
;; => 2227799865N
;; => 2227799865N
;; => 2227799865N
;; => 114N
;; => 215766908N --too low
;; => 219290906N --incorrect answer

(defn part-b []
  (->> (get-input "09-a")
       str/split-lines
       (map parse-line)
       (map lookback)
       (reduce +)))

(part-b)
;; => 1091N
