(ns challenge-14
  (:require
   [util :refer [get-input transpose print-grid]]
   [clojure.string :as str]))

(def sort-seg
  (memoize (fn [fs]
             (let [os (repeat (get fs \O 0) \O)
                   ss (repeat (get fs \. 0) \.)]
               (concat os ss)))))

(defn roll-segment
  ([c]
   (roll-segment :up c))
  ([dir c]
   (let [seg (->> c
                  frequencies
                  sort-seg)]
     (if (= dir :up)
       seg
       (reverse seg)))))

(defn roll-rocks
  ([c]
   (roll-rocks c :up))
  ([c f]
   (loop [col (drop-while #(not= \# %) c)
          stack (roll-segment f (take-while #(not= \# %) c))]
     (if-not (seq col)
       stack
       (let [squares (take-while #(= \# %) col)
             next-seg (->> col
                           (drop (count squares))
                           (take-while #(not= \# %))
                           (roll-segment f))]
         (recur (drop (+ (count squares) (count next-seg)) col)
                (concat stack squares next-seg)))))))

(defn calculate-load [p]
  (->> p
       transpose
       (map reverse)
       (mapcat (partial map-indexed #(if (= %2 \O) (inc %1) 0)))
       (reduce +)))

(defn part-a []
  (->> (get-input "14-a")
       str/split-lines
       transpose
       (map roll-rocks)
       transpose
       calculate-load))

(comment)

(comment (part-a))
;; => 106990

(defn roll-cycle [platform]
  (let [down-fn :down]
    (->> platform
         ;; roll north, up
         transpose
         (map roll-rocks)
         ;; roll west, up
         transpose
         (map roll-rocks)
         ;; roll south, down
         transpose
         (map (fn [r] (roll-rocks r down-fn)))
         ;; roll east, down
         transpose
         (map (fn [r] (roll-rocks r down-fn))))))

(defn rotate-platform [p]
  (lazy-seq
   (let [n (roll-cycle p)]
     (cons n (rotate-platform n)))))

(defn part-b []
  (let [i (->> (get-input "14-a")
               str/split-lines)
        s (->> (rotate-platform i)
               (map calculate-load)
               (take 200))
        freqs (frequencies s)
        chaffe (->> (keep (fn [[k v]] (when (= 1 v) k)) freqs)
                    (into #{}))
        filtered (->> (map #(if-not (chaffe %2) [%1 %2]) (range) s) (remove nil?))
        [idx num] (->> (reverse filtered)
                       (reduce #(if (= (first %1) (inc (first %2)))
                                  %2
                                  (reduced %1))))
        skimmed (drop-while (fn [[i n]] (< i idx)) filtered)
        els (->> skimmed (map second) (into #{}))
        cycles (for [e els
                     :let [c (->> skimmed
                                  (keep (fn [[i n]] (when (= n e) i)))
                                  (remove nil?))]]
                 [e (->> c
                         (partition 2 1 c)
                         (mapv #(apply - (reverse %)))
                         (apply max))])
        [n len] (first (sort-by second > cycles))
        cycle-idx (->> skimmed
                       (drop-while (fn [[_ num]] (not= n num)))
                       ffirst)
        m (mod (- 1000000000 cycle-idx) len)]
    (nth s (+ (dec cycle-idx) m))))
(part-b)
;; => 100531
;; => 131 -- unique elements after 200 iterations; still that many after 400
;; => 50
;; => #{98231 99852 100596 99185 100756 100646 99901 99445 99994 100794 100203 98630 100811 100763 100824 99942 98722 97963 100244 100162 98149 100770 100833 99250 100781 100791 100862 98362 97931 100687 100624 97867 100425 99080 100337 98810 100846 100525 100120 100780 99341 99703 99390 98054 99575 99780 100042 98514 98955 100725}
;; => #{65 69 64 68 87 63}
;; => (87 69 69 69 65 64 65 63 68 69 69 65 64 65 63 68 69 69 65 64 65 63 68 69 69 65 64 65 63 68 69 69 65 64 65 63 68 69 69 65 64 65 63 68 69 69 65 64 65 63)
;; => {87 1, 69 59, 65 56, 64 28, 63 28, 68 28}
;; => {87 1, 69 59, 65 56, 64 28, 63 28, 68 28}
;; => {87 1, 69 59, 65 56, 64 28, 63 28, 68 28}
